
## The node.js test task 

### How to run

`npm install` - Installs dependencies

`npm run app` - Starts the whole application.

`npm run worker` - This is for debug purposes. Starts a single worker (message processor) without api server.
 We may run several instances to test simultaneous access to DB.


### API

Only POST requests are enabled now.

`echoAtTime` - pushes the message
 
parameters:
1. `msg`: message text
2. `time`: unix timestamp in ms, when a message should be processed. 
    * For debugging you may set `debug.echoAtTime_time_startFromCurrent` to `true`
     which means to perceive this parameter as a delay from the current time instead.

Example:
http://localhost:8080/echoAtTime?time=1571700550000&msg=hello%20world


### Redis 

* `messages` - HASH set for storing messages
  
  where `key` is unique ID of the message, `value` is the message text


* `message_schedule` - Sorted set for scheduling message processing
  
  where `value` is the ID of the message, `score` is the unix timestamp in ms when the message have to be processed out
  
* `lock:<messageID>` - the temporary lock item, set to block other worker processors to process the current message.


### Author

Dan (Bohdan Morhuniuk)








