
module.exports = (message) => {
    // just write the message to the server console, according to the task description
    // although here is a possibility to process it in other way
    console.log(`[MESSAGE] ${message}`);
    return true;
};