
const config = rootRequire('config');
const util = rootRequire('util/util');
const uuid = require('uuid/v4');
const redis = require("redis");

const HASH_MESSAGES = 'messages';
const ZSET_SCHEDULE = 'message_schedule';

const UPDATE_CHANNEL = 'messages_update_channel';
const UPDATE_EVENT = 'messages_update_event';

const EXCEPTION_LOCKED = 'locked';

let dbclient = redis.createClient();

function pushMessage(message, time, callback) {
    console.log(`Put message "${message}" for time: ${time}`);

    const messageID = uuid();

    dbclient.multi()
        .hset(HASH_MESSAGES, messageID, message)  // register new message
        .zadd(ZSET_SCHEDULE, time, messageID)     // put the message to the schedule
        .exec((err, replies) => {
            if (err) {
                console.log(`[messageQueue.push] Error. ${JSON.stringify(err)}`);
            } else {
                dbclient.publish(UPDATE_CHANNEL,UPDATE_EVENT);
            }

            if (callback) callback(err);
        });
}

function _checkScheduleForUpcommingMessage() {
    return new Promise((resolve, reject) => {
        // get the oldest message (with the lowest time) within schedule
        dbclient.zrangebyscore(ZSET_SCHEDULE, '-inf', util.currentTime(), 'LIMIT', 0, 1, (err, res) => {
            if (err) {
                reject(err);
                return;
            }

            let messageID = null;
            if (Array.isArray(res) && res.length > 0) {
                messageID = res[0];
            }
            resolve(messageID);
        });
    });
}

function _setMessageLock(messageID) {
    return new Promise((resolve, reject) => {
        // set a lock for other processes attempting to process current message
        // the lock is released automatically after some time in purpose not to block message processing if current processor crashes
        dbclient.set('lock:' + messageID, 1, 'NX', 'PX', config.messageQueue.lockDuration, (err, res) => {
            if (err) { reject(err); return; }

            if (res === 'OK')
                resolve();
            else
                reject(EXCEPTION_LOCKED);
        });
    });
}

function _popMessageByID(messageID, processMessageCallback, cb) {
    return new Promise((resolve, reject) => {
        // set a lock for message processing to avoid other instances to process the message
        _setMessageLock(messageID).then(() => {
            dbclient.hget(HASH_MESSAGES, messageID, (err, message) => { // get the message by ID
                if (err) {
                    reject(err);
                    return;
                }

                if (message && processMessageCallback(message)) { // if the message is processed well
                    dbclient.multi()
                        .zrem(ZSET_SCHEDULE, messageID)  // remove message from the schedule
                        .hdel(HASH_MESSAGES, messageID)  // delete message from the message list
                        .exec(err => {err ? reject(err) : resolve(true)});
                } else {
                    resolve(false);
                }

            });
        }).catch(err => {
            if (err === EXCEPTION_LOCKED)
                resolve(false);
            else
                reject(err);
        });
    });
}

function _processNewMessageIfExist(processMessageCallback) {
    return new Promise((resolve, reject) => {
        _checkScheduleForUpcommingMessage().then(messageID => {
            if (messageID) {
                _popMessageByID(messageID, processMessageCallback).then(resolve).catch(reject);
            } else {
                resolve(false);
            }
        }).catch(reject);
    });
}



function runWorker(processMessageCallback) {
    console.log(`[WORKER] started`);

    const checkForUpdates = () => _processNewMessageIfExist(processMessageCallback).then(processed => {
        if (processed) {
            checkForUpdates(); // check another one
        }
    }).catch(err => {
        console.log(`[WORKER] Error. ${JSON.stringify(err)}`);
    });

    // check for updates on db update
    let subclient = redis.createClient();
    subclient.on('message', (channel, eventMsg) => (channel === UPDATE_CHANNEL && eventMsg === UPDATE_EVENT) && checkForUpdates());
    subclient.subscribe(UPDATE_CHANNEL);

    // check for updates by timer
    setInterval(checkForUpdates, config.messageQueue.workerUpdateInterval);
}

module.exports = {
    pushMessage: pushMessage,
    runWorker: runWorker
};