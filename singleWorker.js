
// pre-initialization
require('./preinit');

// run message queue worker
let messageQueue = require('./messages/messageQueue');
let defaultMessageProcessor = require('./messages/defaultMessageProcessor');
messageQueue.runWorker( defaultMessageProcessor );
