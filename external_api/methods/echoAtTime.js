let messageQueue = rootRequire('messages/messageQueue');
let util = rootRequire('util/util');
let config = rootRequire('config');

function echoAtTime(req, res) {
    let time = parseInt(req.query.time);
    let msg = req.query.msg;
    if (typeof time === 'undefined' || typeof msg === 'undefined') {
        res.status('400').send('Bad request');
        return;
    }

    // DEBUG: perceive the time parameter as delay from current time for easier debugging
    if (config.debug.echoAtTime_time_startFromCurrent) {
        time += util.currentTime();
    }

    console.log('[echoAtTime]', `time: ${time} msg: ${msg}`);

    messageQueue.pushMessage(msg, time, (err) => {
        if (err)
            res.status(500).send('Error');
        else
            res.send('OK');
    });

}


module.exports = echoAtTime;