const express = require('express');
const config = rootRequire('config');

function registerAPI(app,apiName) {
    app.post(`/${apiName}`, require(`./methods/${apiName}`));
    console.log(`API connected: ${apiName}`)
}

function start() {
    let app = express();

    config.apiServer.supportedAPI.forEach(api => registerAPI(app, api));

    let server = app.listen(config.apiServer.port, function () {
        console.log("API server started at port %s", server.address().port)
    });
}

module.exports = {
    start: start
};