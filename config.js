
module.exports = {

    apiServer: {
        port: 8080,
        supportedAPI: ['echoAtTime']
    },

    messageQueue: {
        workerUpdateInterval: 1000,
        lockDuration: 20000
    },

    debug: {
        echoAtTime_time_startFromCurrent: false
    }

};